/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VueRouter from 'vue-router';

window.Vue = require('vue');
window.Vue.use(VueRouter);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
import ThongKe from './components/MainAdmin.vue';
import ListPhong from './components/PhongTro/ListPhongTro.vue';
import CreatePhong from './components/PhongTro/CreatePhongTro.vue';
import EditPhong from './components/PhongTro/EditPhongTro.vue'
import NguoiDung from './components/User/NguoiDung.vue';
import { Form, HasError, AlertError } from 'vform';
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
window.Fire=new Vue();


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const routes = [
    {
        path: '/',
        name: 'main',
        component: ThongKe
    },
    {
        path: '/list-phong',
        name: 'listPhong',
        component: ListPhong
    },
    {
        path: '/create-phong',
        name: 'createPhong',
        component: CreatePhong
    },
    {
        path: '/edit-phong',
        name: 'editPhong',
        component: NguoiDung
    },
    {
        path: '/nguoi-dung',
        name: 'nguoiDung',
        component: NguoiDung
    },

]
const router = new VueRouter({
    routes
})
const app = new Vue({
    el: '#app',
    router
});

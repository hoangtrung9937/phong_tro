<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Support\Facades\DB;

class PhongTroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $phong = DB::table('post')
            ->join('province','province.id','=','post.location_id')
            ->join('users','users.id','=','post.created_by')
            ->select('post.*','province._name as provinceName','users.name as userName')
        ->where('type', '=', 1)->get();
        return $phong;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $strimage='';
        $hinhanh=$request->images;
        if($hinhanh)
        {
            foreach ($hinhanh as $item) {
                $name = time().'.' . explode('/', explode(':', substr($item, 0, strpos($item, ';')))[1])[1];
                \Image::make($item)->save(public_path('ImageUpload/').$name);
                $strimage.=$name;
           }
        $p = new Post();
        $p->code = $request->code;
        $p->type = 1;
        $p->status = 1;
        $p->contact = $request->contact;
        $p->area = $request->area;
        $p->price_monthly = $request->price_monthly;
        $p->full_address = $request->full_address;
        $p->created_by = 12;
        $p->location_id = 1;
        $p->description = $request->dep;
        $p->images = $strimage;
        $p->expired_at = $request->expired;
        $p->save();
        return $p;
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        return 'Delete OK';
    }
}
